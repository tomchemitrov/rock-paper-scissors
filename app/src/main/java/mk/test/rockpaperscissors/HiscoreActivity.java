package mk.test.rockpaperscissors;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HiscoreActivity extends AppCompatActivity {

    TextView playerPointsTV, computerPointsTV;
    Button clear;

    int playerPoints, computerPoints;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hiscore);

        playerPointsTV = findViewById(R.id.playerPoints);
        computerPointsTV = findViewById(R.id.computerPoints);
        clear = findViewById(R.id.clearButton);

        SharedPreferences pref = getSharedPreferences("High_Score",MODE_PRIVATE);
        playerPoints = pref.getInt("playerPoints",-1);
        computerPoints = pref.getInt("computerPoints", -1);

        playerPointsTV.setText("Your score:      " + playerPoints);
        computerPointsTV.setText("Computer score:      " + computerPoints);
    }

    public void onClearClick(View view) {
        playerPoints = 0;
        computerPoints = 0;
        playerPointsTV.setText("Your score:      0");
        computerPointsTV.setText("Computer score:      0");
        finish();

    }
}
