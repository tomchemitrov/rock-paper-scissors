package mk.test.rockpaperscissors;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class ReplayActivity extends AppCompatActivity {

    ImageView rock, paper, scissors;
    TextView playerTV, computerTV, winnerTV;
    Button replay, hiscore;

    String playerWeapon, computerWeapon;
    int playerPoints = 0;
    int computerPoints = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replay);

        rock = findViewById(R.id.rock);
        paper = findViewById(R.id.paper);
        scissors = findViewById(R.id.scissors);

        playerTV = findViewById(R.id.playerSelected);
        computerTV = findViewById(R.id.computerSelected);
        winnerTV = findViewById(R.id.winner);

        replay = findViewById(R.id.replay);
        hiscore = findViewById(R.id.hiscore);

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            playerWeapon = extras.getString("playerWeapon");
            playerTV.setText("You selected       " + playerWeapon.toUpperCase());

            if (playerWeapon.equals("Rock")) rock.setBackgroundColor(Color.parseColor("#008577"));
            else if (playerWeapon.equals("Paper")) paper.setBackgroundColor(Color.parseColor("#008577"));
            else  scissors.setBackgroundColor(Color.parseColor("#008577"));

            String[] weapons ={"Rock", "Paper", "Scissors"};
            computerWeapon = weapons[new Random().nextInt(weapons.length)];
            computerTV.setText("Computer selected       " + computerWeapon.toUpperCase());

            if((playerWeapon.equals("Rock")  && computerWeapon.equals("Scissors")) || (playerWeapon.equals("Scissors") && computerWeapon.equals("Paper")) || (playerWeapon.equals("Paper") && computerWeapon.equals("Rock"))){
                winnerTV.setText("YOU ARE THE WINNER !!! ");
                playerPoints++;
            }
            else if ((playerWeapon.equals("Scissors") && computerWeapon.equals("Rock")) || (playerWeapon.equals("Rock") && computerWeapon.equals("Paper")) || (playerWeapon.equals("Paper") && computerWeapon.equals("Scissors"))){
                winnerTV.setText("THE COMPUTER WINS !!! ");
                computerPoints++;
            }
            else winnerTV.setText("IT'S A TIE !!! ");
        }
    }

    public void onReplayClick(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void onHiscoreClick(View view) {
        SharedPreferences.Editor editor = getSharedPreferences("High_Score",MODE_PRIVATE).edit();
        editor.putInt("playerPoints", playerPoints);
        editor.putInt("computerPoints", computerPoints);
        editor.commit();

        Intent intent = new Intent(this,HiscoreActivity.class);
        startActivity(intent);
    }
}
