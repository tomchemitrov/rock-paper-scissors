package mk.test.rockpaperscissors;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    ImageView rock, paper, scissors;
    TextView playerTV, computerTV, winnerTV;
    Button play;

    String playerWeapon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rock = findViewById(R.id.rock);
        paper = findViewById(R.id.paper);
        scissors = findViewById(R.id.scissors);

        playerTV = findViewById(R.id.playerSelected);
        computerTV = findViewById(R.id.computerSelected);
        winnerTV = findViewById(R.id.winner);
        play = findViewById(R.id.play);

    }

    public void onRockClick(View view) {
        playerWeapon = "Rock";
        playerTV.setText("You selected       ROCK");

        rock.setBackgroundColor(Color.parseColor("#008577"));
        paper.setBackgroundColor(Color.parseColor("#6ED872"));
        scissors.setBackgroundColor(Color.parseColor("#6ED872"));
    }

    public void onPaperClick(View view) {
        playerWeapon = "Paper";
        playerTV.setText("You selected       PAPER");

        paper.setBackgroundColor(Color.parseColor("#008577"));
        rock.setBackgroundColor(Color.parseColor("#6ED872"));
        scissors.setBackgroundColor(Color.parseColor("#6ED872"));
    }

    public void onScissorsClick(View view) {
        playerWeapon = "Scissors";
        playerTV.setText("You selected       SCISSORS");

        scissors.setBackgroundColor(Color.parseColor("#008577"));
        rock.setBackgroundColor(Color.parseColor("#6ED872"));
        paper.setBackgroundColor(Color.parseColor("#6ED872"));
    }

    public void onPlayButton(View view) {
        if(playerWeapon != "" && playerWeapon != null) {
            Intent intent = new Intent(this, ReplayActivity.class);
            intent.putExtra("playerWeapon", playerWeapon);
            startActivity(intent);
        }
        else Toast.makeText(this,"Please select a weapon",Toast.LENGTH_LONG).show();
    }
}
